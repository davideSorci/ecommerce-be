package com.davidesorci.ecommerce.configurations;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.web.servlet.function.RouterFunction;
import org.springframework.web.servlet.function.ServerResponse;
import springfox.documentation.builders.ExampleBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.builders.ResponseBuilder;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.net.URI;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.springframework.web.servlet.function.RequestPredicates.GET;
import static org.springframework.web.servlet.function.RouterFunctions.route;

@EnableSwagger2
@Configuration
public class SwaggerDocumentation {

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.davidesorci.ecommerce.controllers"))
                .paths(PathSelectors.any())
                .build()
                .securitySchemes(Collections.singletonList(apiKey()))
                .securityContexts(securityContext())
                .globalResponses(HttpMethod.GET, globalResponses())
                .globalResponses(HttpMethod.POST, globalResponses())
                .globalResponses(HttpMethod.PUT, globalResponses())
                .globalResponses(HttpMethod.DELETE, globalResponses());
    }

    // ========== Api Key=========
    private ApiKey apiKey() {
        return new ApiKey("apiKey", "Authorization", "header");
    }

    //==== Security context=============
    private List<SecurityContext> securityContext() {
        return List.of(SecurityContext.builder().securityReferences(defaultAuth())
                .forPaths(PathSelectors.any()).build());
    }

    // ================Default Auth===================
    private List<SecurityReference> defaultAuth() {
        AuthorizationScope authorizationScope = new AuthorizationScope(
                "global", "accessEverything");
        AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
        authorizationScopes[0] = authorizationScope;
        return Arrays.asList(new SecurityReference("apiKey",
                authorizationScopes));
    }

    public ApiInfo apiInfo() {
        return (new ApiInfo(
                "Ecommerce Documentation",
                "Full API call documentation",
                "1.0.0",
                "https://davidesorci.it",
                "Davide Sorci",
                "Open source",
                "https://davidesorci.it"
        ));
    }

    @Bean
    RouterFunction<ServerResponse> routerFunction() {
        return route(GET("/doc"), req ->
                ServerResponse.temporaryRedirect(URI.create("swagger-ui/")).build());
    }

    List<Response> globalResponses() {
        return Collections.singletonList(
                new ResponseBuilder()
                        .code("400")
                        .isDefault(true)
                        .description("Errore inserimento dati")
                        .examples(
                                Collections.singletonList(
                                        new ExampleBuilder()
                                                .id("id")
                                                .mediaType("*/*")
                                                .description("Oggetto di errore")
                                                .value("Messaggio di errore")
                                                .build()
                                )
                        )
                        .build()
        );
    }
}
