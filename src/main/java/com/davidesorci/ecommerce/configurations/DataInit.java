package com.davidesorci.ecommerce.configurations;

import com.davidesorci.ecommerce.database.entities.CategoryEntity;
import com.davidesorci.ecommerce.database.entities.RoleEntity;
import com.davidesorci.ecommerce.database.entities.StatusEntity;
import com.davidesorci.ecommerce.database.entities.TagEntity;
import com.davidesorci.ecommerce.database.repositories.CategoryRepository;
import com.davidesorci.ecommerce.database.repositories.RoleRepository;
import com.davidesorci.ecommerce.database.repositories.StatusRepository;
import com.davidesorci.ecommerce.database.repositories.TagRepository;
import com.davidesorci.ecommerce.dtos.user.InitUserDto;
import com.davidesorci.ecommerce.enums.CategoriesEnum;
import com.davidesorci.ecommerce.enums.RoleEnum;
import com.davidesorci.ecommerce.enums.StatusEnum;
import com.davidesorci.ecommerce.enums.TagsEnum;
import com.davidesorci.ecommerce.services.interfaces.AuthenticationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.List;

import static com.davidesorci.ecommerce.enums.CategoriesEnum.*;

@Slf4j
@Component
public class DataInit implements CommandLineRunner {

    @Autowired
    AuthenticationService authenticationService;
    @Autowired
    RoleRepository roleRepository;
    @Autowired
    StatusRepository statusRepository;
    @Autowired
    CategoryRepository categoryRepository;
    @Autowired
    TagRepository tagRepository;

    @Override
    public void run(String... args) throws Exception {

        log.info("- - Inizializzazione Dati - -");

        try {
            initializeDefaultAdminUser();
        } catch (Exception e) {
            log.error("Errore nella creazione dell'utente amministratore o amministratore già creato");
        }
        try {
            initializeRoles();
        } catch (Exception e) {
            log.error("Errore nella creazione dei ruoli o ruoli già creati");
        }
        try {
            initializeStatus();
        } catch (Exception e) {
            log.error("Errore nella creazione degli status o status già creati");
        }
        try {
            initializeCategories();
        } catch (Exception e) {
            log.error("Errore nella creazione delle categorie o categoire già create");
        }
        try {
            initializeTags();
        } catch (Exception e) {
            log.error("Errore nella creazione dei tag o tag già creati");
        }

        log.info("- - Fine Inizializzazione Dati - -");
    }

    private void initializeDefaultAdminUser() {
        InitUserDto initUserDto = new InitUserDto();
        initUserDto.setName("admin");
        initUserDto.setSurname("admin");
        initUserDto.setEmail("admin@admin.com");
        initUserDto.setPhone("+39329887564");
        initUserDto.setPassword("***??!a!d!m!i?n");

        authenticationService.initializeUser(initUserDto, RoleEnum.ADMIN);
    }

    private void initializeRoles() {
        for (RoleEnum roleEnum : RoleEnum.values()) {
            if (roleRepository.existsByName(roleEnum.toString()))
                continue;

            RoleEntity roleEntity = new RoleEntity();
            roleEntity.setName(roleEnum.toString());

            roleRepository.save(roleEntity);
        }
    }

    private void initializeStatus() {
        for (StatusEnum statusEnum : StatusEnum.values()) {
            if (statusRepository.existsByName(statusEnum.toString()))
                continue;

            StatusEntity statusEntity = new StatusEntity();
            statusEntity.setName(statusEnum.toString());

            statusRepository.save(statusEntity);
        }
    }

    private void initializeCategories() {
        CategoryEntity categoryEntity = new CategoryEntity();

        for (CategoriesEnum categoriesEnum : CategoriesEnum.values()) {
            if (categoryRepository.existsByName(categoriesEnum.toString()))
                continue;

            categoryEntity.setName(categoriesEnum.toString());
            categoryEntity.setRating(0);

            categoryRepository.save(categoryEntity);
        }
        List<CategoryEntity> categories = categoryRepository.findAll();
        for (CategoryEntity entity : categories) {
            if (entity.getName().equals(LIBRI.name())) entity.setIcon("fa-thin fa-book");
            else if (entity.getName().equals(ELETTRONICA.name())) entity.setIcon("fa-thin fa-laptop");
            else if (entity.getName().equals(CASA_E_GIARDINO.name())) entity.setIcon("fa-thin fa-house-tree");
            else if (entity.getName().equals(SPORT_E_TURISMO.name())) entity.setIcon("fa-thin fa-bicycle");
            else if (entity.getName().equals(GIOCHI.name())) entity.setIcon("fa-thin fa-gamepad");
            else if (entity.getName().equals(POPOLARI.name())) entity.setIcon("fa-thin fa-star");
            else if (entity.getName().equals(CUCINA.name())) entity.setIcon("fa-thin fa-hat-chef");
            else if (entity.getName().equals(CIBO_E_BEVANDE.name())) entity.setIcon("fa-thin fa-french-fries");
            else if (entity.getName().equals(GIFT_CARDS.name())) entity.setIcon("fa-thin fa-gift-card");
            else if (entity.getName().equals(AUTOMOTIVE.name())) entity.setIcon("fa-thin fa-car");
            else entity.setIcon("");
        }
        categoryRepository.saveAll(categories);
    }

    private void initializeTags() {
        for (TagsEnum tagsEnum : TagsEnum.values()) {
            if (tagRepository.existsByName(tagsEnum.toString()))
                continue;

            TagEntity tagEntity = new TagEntity();
            tagEntity.setName(tagsEnum.toString());

            tagRepository.save(tagEntity);
        }
    }
}
