package com.davidesorci.ecommerce.configurations;

import com.davidesorci.ecommerce.security.EcommerceAuthenticationFilter;
import com.davidesorci.ecommerce.services.interfaces.AuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
public class WebSecurityConfiguration {

    @Autowired
    AuthenticationConfiguration configuration;
    @Autowired
    AuthenticationService authenticationService;

    private static final String[] AUTH_GET_WHITELIST = {
            "/",
            "/swagger-ui/**",
            "/webjars/**",
            "/v2/**",
            "/swagger-resources/**",
            "/doc",
            "/v1/category/getAll",
            "/v1/advertisement/published"
    };

    private static final String[] AUTH_POST_WHITELIST = {
            "/v1/authentication/registration/client",
            "/v1/authentication/registration/seller",
            "/v1/authentication/login"
    };

    private static final String[] AUTH_PATCH_WHITELIST = {
            "/v1/category/updateRating"
    };

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http
                .csrf().disable()
                .authorizeHttpRequests()
                .antMatchers(HttpMethod.GET, AUTH_GET_WHITELIST)
                .permitAll()
                .antMatchers(HttpMethod.POST, AUTH_POST_WHITELIST)
                .permitAll()
                .antMatchers(HttpMethod.PATCH, AUTH_PATCH_WHITELIST)
                .permitAll()
                .anyRequest()
                .authenticated()
                .and()
                .cors()
                .and()
                .addFilter(
                        new EcommerceAuthenticationFilter(
                                authenticationService,
                                configuration.getAuthenticationManager()
                        )
                )
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS);


        return http.build();
    }
}
