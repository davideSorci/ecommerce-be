package com.davidesorci.ecommerce.security;

import com.davidesorci.ecommerce.enums.RoleEnum;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class UserAuthenticated {

    private Long id;
    private String name;
    private RoleEnum role;

}
