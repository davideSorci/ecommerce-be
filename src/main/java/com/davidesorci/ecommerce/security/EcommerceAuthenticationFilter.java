package com.davidesorci.ecommerce.security;

import com.davidesorci.ecommerce.services.interfaces.AuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;

public class EcommerceAuthenticationFilter extends BasicAuthenticationFilter {

    @Autowired
    AuthenticationService authenticationService;

    public EcommerceAuthenticationFilter(AuthenticationService authenticationService, AuthenticationManager authenticationManager) {
        super(authenticationManager);
        this.authenticationService = authenticationService;
    }

    private Authentication getAuthentication(String authorizationHeader) {
        String token = authorizationHeader.replace(SecurityConstants.TOKEN_PREFIX, "");

        var claims = TokenUtils.getClaims(token);
        var userId = Long.parseLong(claims.getSubject());

        var userAuthenticated = authenticationService.getAuthentication(userId);

        return new UsernamePasswordAuthenticationToken(userAuthenticated, null,
                Collections.singleton(new SimpleGrantedAuthority(userAuthenticated.getRole().name())));
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {

        String authorizationHeader = request.getHeader(HttpHeaders.AUTHORIZATION);

        if(authorizationHeader != null) {
            try {
                var auth = getAuthentication(authorizationHeader);
                SecurityContextHolder.getContext().setAuthentication(auth);
            } catch (Exception ignored) {

            }
        }
        chain.doFilter(request, response);
    }
}
