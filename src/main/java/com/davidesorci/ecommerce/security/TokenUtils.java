package com.davidesorci.ecommerce.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;

import javax.crypto.SecretKey;


public class TokenUtils {
    public static SecretKey getSecretKey() {
        return Keys.hmacShaKeyFor(SecurityConstants.TOKEN_SECRET.getBytes());
    }

    public static Claims getClaims(String token) {
        var key = getSecretKey();

        return Jwts.parserBuilder()
                .setSigningKey(key)
                .build()
                .parseClaimsJws(token)
                .getBody();
    }
}
