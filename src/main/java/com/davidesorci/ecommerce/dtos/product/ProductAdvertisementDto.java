package com.davidesorci.ecommerce.dtos.product;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ProductAdvertisementDto {

    private double price;
    private String description;
    private Integer discount;
    private String brand;
    private String title;
    private String imgPath;
}
