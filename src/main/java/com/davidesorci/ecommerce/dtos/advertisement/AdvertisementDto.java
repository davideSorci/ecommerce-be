package com.davidesorci.ecommerce.dtos.advertisement;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class AdvertisementDto {

    private String productBrand;
    private String productTitle;
    private String productOption;
    private double price;
    private double shippingCost;
    private String description;
    private String category;
    private Integer discount;
}
