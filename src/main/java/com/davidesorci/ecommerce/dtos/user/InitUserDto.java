package com.davidesorci.ecommerce.dtos.user;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class InitUserDto {

    private String name;
    private String surname;
    private String email;
    private String phone;
    private String password;
}
