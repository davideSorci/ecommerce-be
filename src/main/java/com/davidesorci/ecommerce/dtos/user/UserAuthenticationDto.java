package com.davidesorci.ecommerce.dtos.user;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Builder
@Getter
@Setter
public class UserAuthenticationDto {

    private String name;
    private Date tokenExpiration;
    private String token;

}
