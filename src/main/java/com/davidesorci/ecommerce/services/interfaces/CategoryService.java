package com.davidesorci.ecommerce.services.interfaces;

import com.davidesorci.ecommerce.dtos.category.CategoryDto;

import java.util.List;

public interface CategoryService {

    List<CategoryDto> getAllCategories();
    void updateCategoryRating(CategoryDto categoryDto);
}
