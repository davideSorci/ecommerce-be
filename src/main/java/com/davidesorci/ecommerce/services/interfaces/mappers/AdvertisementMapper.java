package com.davidesorci.ecommerce.services.interfaces.mappers;

import com.davidesorci.ecommerce.database.entities.AdvertisementEntity;
import com.davidesorci.ecommerce.dtos.advertisement.AdvertisementDto;

public interface AdvertisementMapper {

    AdvertisementEntity advDtoToEntity(AdvertisementDto advertisementDto, long sellerId);
}
