package com.davidesorci.ecommerce.services.interfaces;

import com.davidesorci.ecommerce.dtos.user.UserDto;

import java.util.List;

public interface UserService {

    UserDto getUser(Long userId);
    List<UserDto> getAllUsers();
}
