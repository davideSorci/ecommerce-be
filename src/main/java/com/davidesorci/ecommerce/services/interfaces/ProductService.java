package com.davidesorci.ecommerce.services.interfaces;

import com.davidesorci.ecommerce.dtos.product.ProductDto;

public interface ProductService {

    void addProduct(ProductDto productDto);
}
