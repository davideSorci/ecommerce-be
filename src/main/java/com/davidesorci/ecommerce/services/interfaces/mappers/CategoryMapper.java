package com.davidesorci.ecommerce.services.interfaces.mappers;

import com.davidesorci.ecommerce.database.entities.CategoryEntity;
import com.davidesorci.ecommerce.dtos.category.CategoryDto;

import java.util.List;

public interface CategoryMapper {

    List<CategoryDto> categoryEntityToDto(List<CategoryEntity> categoryEntity);
}
