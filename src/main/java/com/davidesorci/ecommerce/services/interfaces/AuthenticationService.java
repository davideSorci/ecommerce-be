package com.davidesorci.ecommerce.services.interfaces;

import com.davidesorci.ecommerce.database.entities.UserEntity;
import com.davidesorci.ecommerce.dtos.login.LoginDto;
import com.davidesorci.ecommerce.dtos.user.InitUserDto;
import com.davidesorci.ecommerce.dtos.user.UserAuthenticationDto;
import com.davidesorci.ecommerce.enums.RoleEnum;
import com.davidesorci.ecommerce.security.UserAuthenticated;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface AuthenticationService {

    UserAuthenticationDto login(LoginDto loginDto);

    UserAuthenticationDto initializeUser(InitUserDto createUser, RoleEnum role);

    UserAuthenticated getAuthentication(Long userId);

    UserAuthenticationDto successfulAuthentication(UserEntity user);

    long GetLoggedUserId();
}
