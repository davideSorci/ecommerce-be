package com.davidesorci.ecommerce.services.interfaces.mappers;

import com.davidesorci.ecommerce.database.entities.UserEntity;
import com.davidesorci.ecommerce.dtos.user.InitUserDto;
import com.davidesorci.ecommerce.dtos.user.UserDto;

public interface UserMapper {

    UserEntity userDtoToEntity(InitUserDto initUserDto);
    UserDto userEntityToDto(UserEntity userEntity);
}
