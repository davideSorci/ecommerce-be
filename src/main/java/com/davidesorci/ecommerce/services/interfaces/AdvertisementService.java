package com.davidesorci.ecommerce.services.interfaces;

import com.davidesorci.ecommerce.dtos.advertisement.AdvertisementDto;
import com.davidesorci.ecommerce.dtos.product.ProductAdvertisementDto;

import java.util.List;

public interface AdvertisementService {

    void addAdvertisement(AdvertisementDto advertisementDto, long sellerId);

    List<ProductAdvertisementDto> getAllProductsWithArticleAssociated();
}
