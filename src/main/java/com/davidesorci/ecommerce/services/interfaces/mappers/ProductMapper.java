package com.davidesorci.ecommerce.services.interfaces.mappers;

import com.davidesorci.ecommerce.database.entities.ProductEntity;
import com.davidesorci.ecommerce.dtos.product.ProductDto;

public interface ProductMapper {

    ProductEntity productDtoToEntity(ProductDto productDto);
}
