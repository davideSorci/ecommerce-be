package com.davidesorci.ecommerce.services.implementations;

import com.davidesorci.ecommerce.database.entities.ProductEntity;
import com.davidesorci.ecommerce.database.repositories.ProductRepository;
import com.davidesorci.ecommerce.dtos.product.ProductDto;
import com.davidesorci.ecommerce.services.implementations.mappers.ProductMapperImplementation;
import com.davidesorci.ecommerce.services.interfaces.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProductServiceImplementation implements ProductService {

    @Autowired
    ProductRepository productRepository;
    @Autowired
    ProductMapperImplementation productMapper;

    @Override
    public void addProduct(ProductDto productDto) {
        if (productDto.getTitle() == null || productDto.getDescription() == null || productDto.getCategory() == null) {
            throw new RuntimeException("Errore nell'inserimento di un nuovo prodotto");
        }
        productRepository.save(productMapper.productDtoToEntity(productDto));
    }
}
