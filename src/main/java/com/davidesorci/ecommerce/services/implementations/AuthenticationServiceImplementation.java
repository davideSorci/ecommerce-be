package com.davidesorci.ecommerce.services.implementations;

import com.davidesorci.ecommerce.database.entities.ClientEntity;
import com.davidesorci.ecommerce.database.entities.RoleEntity;
import com.davidesorci.ecommerce.database.entities.SellerEntity;
import com.davidesorci.ecommerce.database.entities.UserEntity;
import com.davidesorci.ecommerce.database.repositories.ClientRepository;
import com.davidesorci.ecommerce.database.repositories.RoleRepository;
import com.davidesorci.ecommerce.database.repositories.SellerRepository;
import com.davidesorci.ecommerce.database.repositories.UserRepository;
import com.davidesorci.ecommerce.dtos.login.LoginDto;
import com.davidesorci.ecommerce.dtos.user.InitUserDto;
import com.davidesorci.ecommerce.dtos.user.UserAuthenticationDto;
import com.davidesorci.ecommerce.enums.RoleEnum;
import com.davidesorci.ecommerce.security.SecurityConstants;
import com.davidesorci.ecommerce.security.UserAuthenticated;
import com.davidesorci.ecommerce.services.implementations.mappers.UserMapperImplementation;
import com.davidesorci.ecommerce.services.interfaces.AuthenticationService;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.logout.CookieClearingLogoutHandler;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.security.web.authentication.rememberme.AbstractRememberMeServices;
import org.springframework.stereotype.Service;

import javax.crypto.SecretKey;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;

@Service
public class AuthenticationServiceImplementation implements AuthenticationService {

    @Autowired
    BCryptPasswordEncoder encoder;
    @Autowired
    UserRepository userRepository;
    @Autowired
    RoleRepository roleRepository;
    @Autowired
    ClientRepository clientRepository;
    @Autowired
    SellerRepository sellerRepository;
    @Autowired
    UserMapperImplementation userMapper;

    @Override
    public UserAuthenticationDto login(LoginDto loginDto) {

        String email = loginDto.getEmail();
        UserEntity user = userRepository.findByEmail(email)
                .orElseThrow(() -> new RuntimeException("Utente inesistente!"));

        if (!encoder.matches(loginDto.getPassword(), user.getPassword()))
            throw new RuntimeException("Password errata");

        return successfulAuthentication(user);
    }

    @Override
    public UserAuthenticationDto initializeUser(InitUserDto user, RoleEnum role) {

        RoleEntity roleEntity = roleRepository.findAllByName(role.name()).orElseThrow(
                () -> new RuntimeException("Non esiste un ruolo con questo nome " + role.name()));

        UserEntity entity = userMapper.userDtoToEntity(user);
        entity.setRole(roleEntity);
        entity.setPassword(encoder.encode(user.getPassword()));
        userRepository.save(entity);

        if (role.equals(RoleEnum.CLIENT)) {
            ClientEntity client = new ClientEntity();
            client.setUser(entity);
            clientRepository.save(client);
        } else
        if (role.equals(RoleEnum.SELLER)) {
            SellerEntity seller = new SellerEntity();
            seller.setUser(entity);
            sellerRepository.save(seller);
        }

        return successfulAuthentication(entity);
    }

    @Override
    public UserAuthenticated getAuthentication(Long userId) {

        UserEntity user = userRepository.findById(userId).orElseThrow(
                () -> new RuntimeException("Non esiste più l'utente in uso"));

        return UserAuthenticated.builder()
                .id(user.getId())
                .name(user.getName())
                .role(RoleEnum.valueOf(user.getRole().getName()))
                .build();
    }

    @Override
    public UserAuthenticationDto successfulAuthentication(UserEntity user) {

        var userAuthenticated = this.generateToken(user.getId());
        userAuthenticated.setName(user.getName());

        return userAuthenticated;
    }

    @Override
    public long GetLoggedUserId() {

        var loggedUser = (UserAuthenticated) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return loggedUser.getId();
    }

    private UserAuthenticationDto generateToken(Long userId) {

        var now = Date.from(Instant.now());
        var tokenExpiration = Date.from(Instant.now().plus(8, ChronoUnit.HOURS));

        // Token generation
        SecretKey secretKey = Keys.hmacShaKeyFor(SecurityConstants.TOKEN_SECRET.getBytes());
        String token = Jwts.builder()
                .signWith(secretKey, SignatureAlgorithm.HS512)
                .setIssuedAt(now)
                .setNotBefore(now)
                .setExpiration(tokenExpiration)
                .setSubject(userId.toString())
                .compact();

        return UserAuthenticationDto.builder()
                .tokenExpiration(tokenExpiration)
                .token(token)
                .build();
    }
}
