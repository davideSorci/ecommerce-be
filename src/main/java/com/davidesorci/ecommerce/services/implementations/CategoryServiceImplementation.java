package com.davidesorci.ecommerce.services.implementations;

import com.davidesorci.ecommerce.database.entities.CategoryEntity;
import com.davidesorci.ecommerce.database.repositories.CategoryRepository;
import com.davidesorci.ecommerce.dtos.category.CategoryDto;
import com.davidesorci.ecommerce.services.implementations.mappers.CategoryMapperImplementation;
import com.davidesorci.ecommerce.services.interfaces.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImplementation implements CategoryService {

    @Autowired
    CategoryRepository categoryRepository;
    @Autowired
    CategoryMapperImplementation categoryMapper;

    @Override
    public List<CategoryDto> getAllCategories() {

        return categoryMapper.categoryEntityToDto(categoryRepository.findAll());
    }

    @Override
    public void updateCategoryRating(CategoryDto categoryDto) {
        CategoryEntity categoryEntity = categoryRepository.findByName(categoryDto.getName()).get();
        categoryEntity.setRating(categoryEntity.getRating() + 1);
        categoryRepository.save(categoryEntity);
    }
}
