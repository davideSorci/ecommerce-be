package com.davidesorci.ecommerce.services.implementations.mappers;

import com.davidesorci.ecommerce.database.entities.RoleEntity;
import com.davidesorci.ecommerce.database.entities.UserEntity;
import com.davidesorci.ecommerce.database.repositories.RoleRepository;
import com.davidesorci.ecommerce.dtos.user.InitUserDto;
import com.davidesorci.ecommerce.dtos.user.UserDto;
import com.davidesorci.ecommerce.services.interfaces.mappers.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserMapperImplementation implements UserMapper {

    @Autowired
    RoleRepository roleRepository;

    @Override
    public UserEntity userDtoToEntity(InitUserDto initUserDto) {

        UserEntity userEntity = new UserEntity();
        userEntity.setName(initUserDto.getName());
        userEntity.setSurname(initUserDto.getSurname());
        userEntity.setEmail(initUserDto.getEmail());
        userEntity.setPhone(initUserDto.getPhone());
        userEntity.setPassword(initUserDto.getPassword());
        return userEntity;
    }

    @Override
    public UserDto userEntityToDto(UserEntity userEntity) {

        RoleEntity role = roleRepository.findById(userEntity.getRole().getId()).orElseThrow(
                () -> new RuntimeException("Status non trovato"));

        UserDto userDto = new UserDto();
        userDto.setName(userEntity.getName());
        userDto.setSurname(userEntity.getSurname());
        userDto.setEmail(userEntity.getEmail());
        userDto.setPhone(userEntity.getPhone());
        userDto.setRole(role.getName());
        return userDto;
    }
}
