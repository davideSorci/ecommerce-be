package com.davidesorci.ecommerce.services.implementations.mappers;

import com.davidesorci.ecommerce.database.entities.CategoryEntity;
import com.davidesorci.ecommerce.database.entities.ProductEntity;
import com.davidesorci.ecommerce.database.repositories.CategoryRepository;
import com.davidesorci.ecommerce.dtos.product.ProductDto;
import com.davidesorci.ecommerce.services.interfaces.mappers.ProductMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProductMapperImplementation implements ProductMapper {

    @Autowired
    CategoryRepository categoryRepository;

    @Override
    public ProductEntity productDtoToEntity(ProductDto productDto) {
        ProductEntity productEntity = new ProductEntity();

        CategoryEntity category = categoryRepository.findById(productEntity.getId()).orElseThrow(
                () -> new RuntimeException("Categoria non trovata"));

        productEntity.setTitle(productDto.getTitle());
        productEntity.setCategory(category);
        return productEntity;
    }
}
