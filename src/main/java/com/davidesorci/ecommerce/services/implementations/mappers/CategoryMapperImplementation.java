package com.davidesorci.ecommerce.services.implementations.mappers;

import com.davidesorci.ecommerce.database.entities.CategoryEntity;
import com.davidesorci.ecommerce.dtos.category.CategoryDto;
import com.davidesorci.ecommerce.services.interfaces.mappers.CategoryMapper;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryMapperImplementation implements CategoryMapper {
    @Override
    public List<CategoryDto> categoryEntityToDto(List<CategoryEntity> categoryEntity) {

        List<CategoryDto> categoryDtoList = categoryEntity.stream().map(
                entity -> {
                    CategoryDto categoryDto = new CategoryDto();
                    categoryDto.setName(entity.getName());
                    categoryDto.setRating(entity.getRating());
                    categoryDto.setDescription(entity.getDescription());
                    categoryDto.setIcon(entity.getIcon());
                    return categoryDto;
                }
        ).toList();

        return categoryDtoList;
    }
}
