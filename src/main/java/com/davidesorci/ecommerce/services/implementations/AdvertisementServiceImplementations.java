package com.davidesorci.ecommerce.services.implementations;

import com.davidesorci.ecommerce.database.entities.AdvertisementEntity;
import com.davidesorci.ecommerce.database.entities.ProductEntity;
import com.davidesorci.ecommerce.database.repositories.AdvertisementRepository;
import com.davidesorci.ecommerce.database.repositories.ProductRepository;
import com.davidesorci.ecommerce.dtos.advertisement.AdvertisementDto;
import com.davidesorci.ecommerce.dtos.product.ProductAdvertisementDto;
import com.davidesorci.ecommerce.services.implementations.mappers.AdvertisementMapperImplementation;
import com.davidesorci.ecommerce.services.interfaces.AdvertisementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class AdvertisementServiceImplementations implements AdvertisementService {

    @Autowired
    AdvertisementRepository advertisementRepository;
    @Autowired
    ProductRepository productRepository;
    @Autowired
    AdvertisementMapperImplementation mapper;

    @Override
    public void addAdvertisement(AdvertisementDto advertisementDto, long sellerId) {
        advertisementRepository.save(mapper.advDtoToEntity(advertisementDto, sellerId));
    }

    @Override
    public List<ProductAdvertisementDto> getAllProductsWithArticleAssociated() {

        List<ProductAdvertisementDto> prodAdvDtos = new ArrayList<>();
        List<AdvertisementEntity> advertisementEntities = advertisementRepository.findAll();

        advertisementEntities.forEach(advEntity -> {
            ProductAdvertisementDto prodAdvDto = new ProductAdvertisementDto();
            prodAdvDto.setPrice(advEntity.getPrice());
            prodAdvDto.setDescription(advEntity.getDescription());
            prodAdvDto.setDiscount(advEntity.getDiscount());
            ProductEntity product = productRepository.findById(advEntity.getProduct().getId()).orElseThrow(
                    () -> new RuntimeException("No product found")
            );
            prodAdvDto.setBrand(product.getBrand());
            prodAdvDto.setTitle(product.getTitle());
            prodAdvDto.setImgPath(product.getImgPath());
            prodAdvDtos.add(prodAdvDto);
        });

        return prodAdvDtos;
    }
}
