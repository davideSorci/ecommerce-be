package com.davidesorci.ecommerce.services.implementations.mappers;

import com.davidesorci.ecommerce.database.entities.*;
import com.davidesorci.ecommerce.database.repositories.*;
import com.davidesorci.ecommerce.dtos.advertisement.AdvertisementDto;
import com.davidesorci.ecommerce.services.interfaces.mappers.AdvertisementMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static com.davidesorci.ecommerce.enums.StatusEnum.PUBBLICATO;

@Service
public class AdvertisementMapperImplementation implements AdvertisementMapper {

    @Autowired
    ProductRepository productRepository;
    @Autowired
    SellerRepository sellerRepository;
    @Autowired
    StatusRepository statusRepository;
    @Autowired
    CategoryRepository categoryRepository;

    @Override
    public AdvertisementEntity advDtoToEntity(AdvertisementDto advertisementDto, long sellerId) {
        AdvertisementEntity advertisementEntity = new AdvertisementEntity();

        advertisementEntity.setPrice(advertisementDto.getPrice());
        advertisementEntity.setShippingCost(advertisementDto.getShippingCost());
        advertisementEntity.setClient(null);
            ProductEntity product = new ProductEntity();
            product.setBrand(advertisementDto.getProductBrand().toUpperCase());
            CategoryEntity category = categoryRepository.findByName(advertisementDto.getCategory()).orElseThrow(
                    () -> new RuntimeException("Categoria non trovata")
            );
            product.setCategory(category);
            product.setArticleName(advertisementDto.getProductTitle().toUpperCase());
            if (advertisementDto.getProductOption() != null) {
                product.setOtherOptions(advertisementDto.getProductOption().toUpperCase());
            } else {
                product.setOtherOptions("");
            }
            product.setTitle(advertisementDto.getProductBrand() + " " + advertisementDto.getProductTitle());
            String globalName = product.getBrand() + "_" + product.getTitle() + "_" + product.getOtherOptions();
            product.setImgPath("/" + globalName + ".jpg");
            product.setGlobalName(globalName);
            // check if product already exist
            if(productRepository.findByGlobalName(product.getGlobalName()).isEmpty()) {
                productRepository.save(product);
            } else {
                productRepository.flush();
            }
        advertisementEntity.setProduct(product);
        SellerEntity seller = sellerRepository.findById(sellerId).orElseThrow(
                () -> new RuntimeException("Seller not found")
        );
        advertisementEntity.setSeller(seller);
        StatusEntity status = statusRepository.findAllByName(PUBBLICATO.name()).orElseThrow(
                () -> new RuntimeException("Status non trovato")
        );
        advertisementEntity.setStatus(status);
        advertisementEntity.setDescription(advertisementDto.getDescription());
        advertisementEntity.setDiscount(advertisementDto.getDiscount());
        return advertisementEntity;
    }
}
