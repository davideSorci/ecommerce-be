package com.davidesorci.ecommerce.services.implementations;

import com.davidesorci.ecommerce.database.entities.UserEntity;
import com.davidesorci.ecommerce.database.repositories.UserRepository;
import com.davidesorci.ecommerce.dtos.user.UserDto;
import com.davidesorci.ecommerce.services.implementations.mappers.UserMapperImplementation;
import com.davidesorci.ecommerce.services.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserServiceImplementation implements UserService {

    @Autowired
    UserRepository userRepository;
    @Autowired
    UserMapperImplementation userMapper;

    @Override
    public UserDto getUser(Long userId) {

        UserEntity userEntity = userRepository.findById(userId).orElseThrow(
                () -> new RuntimeException("User not found"));

        return userMapper.userEntityToDto(userEntity);
    }

    @Override
    public List<UserDto> getAllUsers() {

        List<UserEntity> usersEntitiesList = userRepository.findAll();

        return usersEntitiesList.stream().map(
                userEntity -> userMapper.userEntityToDto(userEntity)).collect(Collectors.toList());
    }
}