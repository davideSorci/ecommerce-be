package com.davidesorci.ecommerce.enums;

public enum TagsEnum {

    PC,
    NOTEBOOK,
    SMARTPHONE,
    HEADPHONE
}
