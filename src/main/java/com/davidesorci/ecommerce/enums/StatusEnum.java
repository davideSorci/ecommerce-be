package com.davidesorci.ecommerce.enums;

public enum StatusEnum {

    VENDUTO,
    PUBBLICATO,
    BOZZA
}
