package com.davidesorci.ecommerce.enums;

public enum RoleEnum {

    ADMIN,
    CLIENT,
    SELLER
}
