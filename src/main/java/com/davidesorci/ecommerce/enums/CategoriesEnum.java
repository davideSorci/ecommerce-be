package com.davidesorci.ecommerce.enums;

public enum CategoriesEnum {

    ECHO_E_ALEXA,
    KINDLE,
    LIBRI,
    ELETTRONICA,
    CASA_E_GIARDINO,
    SALUTE_E_BENESSERE,
    BELLEZZA,
    AUTOMOTIVE,
    SPORT_E_TURISMO,
    GIOCHI,
    FILM_E_MUSICA,
    ANIMALI,
    GIFT_CARDS,
    POPOLARI,
    CUCINA,
    CIBO_E_BEVANDE,
    MODA
}
