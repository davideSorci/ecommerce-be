package com.davidesorci.ecommerce.controllers;

import com.davidesorci.ecommerce.dtos.product.ProductDto;
import com.davidesorci.ecommerce.dtos.user.UserDto;
import com.davidesorci.ecommerce.services.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v1/user")
@CrossOrigin(origins = "*")
public class UserController {

    @Autowired
    UserService userService;

    @PreAuthorize("hasAuthority('ADMIN')")
    @GetMapping("/get/{userId}")
    public UserDto getUser(@PathVariable Long userId) {
        return userService.getUser(userId);
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @GetMapping("/getAll")
    public List<UserDto> getAllUsers()  {
        return userService.getAllUsers();
    }
}
