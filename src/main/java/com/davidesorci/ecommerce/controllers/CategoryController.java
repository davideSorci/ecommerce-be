package com.davidesorci.ecommerce.controllers;

import com.davidesorci.ecommerce.database.entities.CategoryEntity;
import com.davidesorci.ecommerce.dtos.category.CategoryDto;
import com.davidesorci.ecommerce.services.interfaces.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.davidesorci.ecommerce.enums.CategoriesEnum.LIBRI;

@RestController
@RequestMapping("/v1/category")
@CrossOrigin(origins = "*")
public class CategoryController {

    @Autowired
    CategoryService categoryService;

    @GetMapping(value = "/getAll")
    public List<CategoryDto> getAllCategories() {
        return categoryService.getAllCategories();
    }
    @PatchMapping("/updateRating")
    public void updateCategoryRating(@RequestBody CategoryDto categoryDto) {
        categoryService.updateCategoryRating(categoryDto);
    }
}
