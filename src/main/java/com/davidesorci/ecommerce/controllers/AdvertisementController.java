package com.davidesorci.ecommerce.controllers;

import com.davidesorci.ecommerce.dtos.advertisement.AdvertisementDto;
import com.davidesorci.ecommerce.dtos.product.ProductAdvertisementDto;
import com.davidesorci.ecommerce.services.interfaces.AdvertisementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v1/advertisement")
@CrossOrigin(origins = "*")
public class AdvertisementController {

    @Autowired
    AdvertisementService advertisementService;

    @PostMapping(value = "/add/{sellerId}")
    @PreAuthorize("hasAuthority('SELLER')")
    public void addProduct(@RequestBody AdvertisementDto advertisementDto, @PathVariable long sellerId) {
        advertisementService.addAdvertisement(advertisementDto, sellerId);
    }

    @GetMapping(value = "/published")
    public List<ProductAdvertisementDto> getAllPublishedProductsWithArticleAssociated() {
        return advertisementService.getAllProductsWithArticleAssociated();
    }
}
