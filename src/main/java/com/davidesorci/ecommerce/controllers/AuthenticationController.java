package com.davidesorci.ecommerce.controllers;

import com.davidesorci.ecommerce.dtos.login.LoginDto;
import com.davidesorci.ecommerce.dtos.user.InitUserDto;
import com.davidesorci.ecommerce.dtos.user.UserAuthenticationDto;
import com.davidesorci.ecommerce.enums.RoleEnum;
import com.davidesorci.ecommerce.services.interfaces.AuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/v1/authentication")
@CrossOrigin(origins = "*")
public class AuthenticationController {

    @Autowired
    AuthenticationService authenticationService;

    @PostMapping(value = "/registration/client")
    public UserAuthenticationDto initializeClient(@RequestBody InitUserDto userDto){
        return authenticationService.initializeUser(userDto, RoleEnum.CLIENT);
    }

    @PostMapping(value = "/registration/seller")
    public UserAuthenticationDto initializeSeller(@RequestBody InitUserDto userDto){
        return authenticationService.initializeUser(userDto, RoleEnum.SELLER);
    }

    @PostMapping(value = "/login")
    public UserAuthenticationDto loginUser(@RequestBody LoginDto loginDto) {
        return authenticationService.login(loginDto);
    }
}
