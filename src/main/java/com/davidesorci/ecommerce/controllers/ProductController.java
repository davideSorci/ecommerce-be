package com.davidesorci.ecommerce.controllers;

import com.davidesorci.ecommerce.dtos.product.ProductDto;
import com.davidesorci.ecommerce.services.interfaces.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/v1/product")
@CrossOrigin(origins = "*")
public class ProductController {

    @Autowired
    ProductService productService;

    @PostMapping(value = "/add")
    @PreAuthorize("hasAuthority('ADMIN')")
    public void addProduct(@RequestBody ProductDto productDto) {
        productService.addProduct(productDto);
    }
}
