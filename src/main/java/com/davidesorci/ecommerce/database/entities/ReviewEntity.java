package com.davidesorci.ecommerce.database.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "reviews")
public class ReviewEntity extends BaseEntity {

    @Column(nullable = true)
    private int rating;
    @Column(nullable = true)
    private String title;
    @Column(nullable = true)
    private String description;

    @ManyToOne
    @JoinColumn(name = "product_id")
    private ProductEntity product;
    @ManyToOne
    @JoinColumn(name = "client_id")
    private ClientEntity client;
}
