package com.davidesorci.ecommerce.database.entities;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "categories")
public class CategoryEntity extends BaseEntity {

    @Column(nullable = false)
    private String name;
    private long rating;
    private String description;
    private String icon;
    @OneToMany(mappedBy = "category")
    private List<ProductEntity> products;
}
