package com.davidesorci.ecommerce.database.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "clients")
public class ClientEntity {

    @Id
    @Column(name = "user_id")
    private Long id;

    @OneToOne(optional = false)
    @MapsId
    @JoinColumn(unique = true)
    private UserEntity user;
    @OneToMany(mappedBy = "client")
    private List<AdvertisementEntity> advertisement;
    @OneToMany(mappedBy = "client")
    private List<ReviewEntity> review;
}
