package com.davidesorci.ecommerce.database.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "sellers")
public class SellerEntity {

    @Id
    @Column(name = "user_id")
    private Long id;

    @OneToOne(optional = false)
    @MapsId
    @JoinColumn(unique = true)
    private UserEntity user;
    @OneToMany(mappedBy = "seller")
    private List<AdvertisementEntity> advertisement;
}
