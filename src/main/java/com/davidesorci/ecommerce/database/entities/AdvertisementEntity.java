package com.davidesorci.ecommerce.database.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "advertisements")
public class AdvertisementEntity extends BaseEntity {

    @Column(nullable = false)
    private double price;
    @Column(nullable = false)
    private double shippingCost;
    private String description;
    @Min(value=0)
    @Max(value=100)
    private Integer discount;

    @ManyToOne
    @JoinColumn(name = "seller_id")
    private SellerEntity seller;
    @ManyToOne
    @JoinColumn(name = "client_id")
    private ClientEntity client;
    @ManyToOne
    @JoinColumn(name = "status_id")
    private StatusEntity status;
    @ManyToOne(cascade= CascadeType.ALL)
    @JoinColumn(name = "product_id")
    private ProductEntity product;
}
