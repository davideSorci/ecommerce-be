package com.davidesorci.ecommerce.database.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "products")
public class ProductEntity extends BaseEntity {

    @Column(nullable = false)
    private String brand;
    @Column(nullable = false)
    private String articleName;
    private String otherOptions;
    @Column(nullable = false)
    private String title;
    @Column(unique = true, nullable = false)
    private String globalName;
    private String imgPath;
    private long unitSold;

    @OneToMany(mappedBy = "product")
    private List<AdvertisementEntity> advertisement;
    @OneToMany(mappedBy = "product")
    private List<ReviewEntity> review;
    @ManyToOne
    @JoinColumn(name = "category_id")
    private CategoryEntity category;
    @ManyToMany
    @JoinTable(
            name = "products_tags",
            joinColumns = @JoinColumn(name = "product_id"),
            inverseJoinColumns = @JoinColumn(name = "tag_id")
    )
    private List<TagEntity> tag;
}
