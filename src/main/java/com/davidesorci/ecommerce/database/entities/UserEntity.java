package com.davidesorci.ecommerce.database.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "users")
public class UserEntity extends BaseEntity {

    @Column(nullable = false)
    private String name;
    @Column(nullable = false)
    private String surname;
    @Column(unique = true, nullable = false)
    private String email;
    @Column(unique = true, nullable = false, length = 13)
    private String phone;
    @Column(nullable = false)
    private String password;

    @OneToOne(mappedBy = "user")
    @PrimaryKeyJoinColumn
    private ClientEntity client;
    @OneToOne(mappedBy = "user")
    @PrimaryKeyJoinColumn
    private SellerEntity seller;
    @ManyToOne
    private RoleEntity role;
}
