package com.davidesorci.ecommerce.database.repositories;

import com.davidesorci.ecommerce.database.entities.TagEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TagRepository extends JpaRepository<TagEntity, Long> {

    boolean existsByName(String name);
}
