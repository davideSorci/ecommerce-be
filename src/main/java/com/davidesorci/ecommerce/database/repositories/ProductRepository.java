package com.davidesorci.ecommerce.database.repositories;

import com.davidesorci.ecommerce.database.entities.ProductEntity;
import com.davidesorci.ecommerce.database.entities.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ProductRepository extends JpaRepository<ProductEntity, Long> {

    Optional<ProductEntity> findByGlobalName(String globalName);
}
