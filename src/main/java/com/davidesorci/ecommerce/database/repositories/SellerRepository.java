package com.davidesorci.ecommerce.database.repositories;

import com.davidesorci.ecommerce.database.entities.SellerEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SellerRepository extends JpaRepository<SellerEntity, Long> {
}
