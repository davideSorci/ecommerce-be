package com.davidesorci.ecommerce.database.repositories;

import com.davidesorci.ecommerce.database.entities.StatusEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface StatusRepository extends JpaRepository<StatusEntity, Long> {

    Optional<StatusEntity> findAllByName(String name);
    boolean existsByName(String name);
}
