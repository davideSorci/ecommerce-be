package com.davidesorci.ecommerce.database.repositories;

import com.davidesorci.ecommerce.database.entities.AdvertisementEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AdvertisementRepository extends JpaRepository<AdvertisementEntity, Long> {
}
