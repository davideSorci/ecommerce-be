package com.davidesorci.ecommerce.database.repositories;

import com.davidesorci.ecommerce.database.entities.RoleEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<RoleEntity, Long> {

    Optional<RoleEntity> findByName(String name);
    boolean existsByName(String name);
    Optional<RoleEntity> findAllByName(String name);
}
