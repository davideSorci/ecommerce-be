package com.davidesorci.ecommerce.database.repositories;

import com.davidesorci.ecommerce.database.entities.ClientEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientRepository extends JpaRepository<ClientEntity, Long> {
}
