FROM openjdk:17
ADD target/ecommerce-0.1.1.jar ecommerce-0.1.1.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "ecommerce-0.1.1.jar"]